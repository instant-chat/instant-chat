import { task } from 'ember-concurrency';
import { inject as service } from '@ember/service';
import { getOwner } from '@ember/application';
import sodium from 'libsodium-wrappers';

export default function() {
  return task({
    chatServer: service(),

    *perform ({ secretText, ttl }) {
      let processor = getOwner(this)
        .factoryFor('message-processor:secret')
        .create({ payload: secretText });
      let payload = processor.encrypt();
      let uuid = processor.uuid;
      let secretKey = sodium.to_base64(processor.secretKey);
      processor.destroy();
      let { exp } = yield this.chatServer.saveSecret({ uuid, ttl, payload });
      return { uuid, secretKey, exp };
    }
  });
}
