import { task, waitForEvent } from 'ember-concurrency';
import { inject as service } from '@ember/service';
import { getOwner } from '@ember/application';
import { isGoneError } from 'ember-ajax/errors';
import sodium from 'libsodium-wrappers';
import { EXCHANGE_REPLY, EXCHANGE_REQUEST } from '../libs/message-types';
import { ExpiredLinkError } from '../libs/errors';
import { serializeUint8Array, deserializeUint8Array }
  from '../utils/primitive-utils';

function eventFor(type) { return `message:${type}`; }

export default function() {
  return task({
    chatServer: service(),
    chatMediator: service(),
    currentUser: service(),
    store: service(),

    sendExchangeRequest(toAddress, pubKey) {
      let fromClient = this.currentUser.client;
      let payload = fromClient.serialize({ includeId: true });
      let processor = getOwner(this)
        .factoryFor('message-processor:exchange')
        .create({ pubKey });
      payload.meta = { authNonce: serializeUint8Array(processor.authNonce) };
      processor.set('payload', payload);
      let xdata = { [toAddress]: processor.encrypt() };
      this.chatServer.sendMessage(EXCHANGE_REQUEST, fromClient.id, xdata);
      this.set('ephemeralKey', pubKey);
      this.set('authNonce', processor.authNonce);
      processor.destroy();
    },

    processExchangeReply({ from, xdata }) {
      let toClient = this.currentUser.client;
      let payload = xdata[toClient.id];
      let nonce = this.authNonce;
      let pubKey = this.ephemeralKey;
      let processor = getOwner(this)
        .factoryFor('message-processor:exchange-reply')
        .create({ toClient, payload, nonce });
      this.store.pushPayload(processor.decrypt(pubKey));
      this.set('client', this.store.peekRecord('client', from));
      this.currentUser.generateMissingSessionKeys({ isServer: true });
      this.chatMediator.set('isLinked', true);
      processor.destroy();
      return this.client;
    },

    *perform (link) {
      const { chatServer } = this;
      let response;
      try {
        response = yield chatServer.fetchInfoForLink(link);
      } catch(error) {
        if (!isGoneError(error)) { throw error; }
        throw new ExpiredLinkError(link);
      }
      let { room, from, pubKey } = response;
      chatServer.joinRoom(room);
      this.sendExchangeRequest(from, deserializeUint8Array(pubKey));
      let exchange = yield waitForEvent(chatServer, eventFor(EXCHANGE_REPLY));
      sodium.increment(this.authNonce);
      return this.processExchangeReply(exchange);
    }
  });
}
