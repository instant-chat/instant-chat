import { task, timeout, race, waitForEvent } from 'ember-concurrency';
import { inject as service } from '@ember/service';
import { getOwner } from '@ember/application';
import sodium from 'libsodium-wrappers';
import { CLIENT_UPDATE, EXCHANGE_REQUEST, EXCHANGE_REPLY }
  from '../libs/message-types';
import { serializeUint8Array, deserializeUint8Array }
  from '../utils/primitive-utils';
import { PairingOfferExpiredError } from '../libs/errors';

function eventFor(type) { return `message:${type}`; }

export default function() {
  return task({
    chatMediator: service(),
    chatServer: service(),
    currentUser: service(),
    store: service(),

    processExchange(exchange) {
      if (!exchange) { throw new PairingOfferExpiredError(); }
      let { from, xdata } = exchange;
      let toClient = this.currentUser.client;
      let payload = xdata[toClient.id];
      let processor = getOwner(this)
        .factoryFor('message-processor:exchange')
        .create({ toClient, payload });
      let json = processor.decrypt();
      this.set('authNonce', deserializeUint8Array(json.meta.authNonce));
      this.set('from', from);
      this.store.pushPayload(json);
      this.set('client', this.store.peekRecord('client', from));
      this.currentUser.generateClientSessionKeys(this.client);
      processor.destroy();
    },

    sendExchangeReply() {
      let toClient = this.client;
      let fromClient = this.currentUser.client;
      let data = this.store.peekAll('client')
        .filter(client => client.id !== toClient.id)
        .map(client => client.serialize({ includeId: true }).data);
      let payload = { data };
      let nonce = this.authNonce;
      let processor = getOwner(this)
        .factoryFor('message-processor:exchange-reply')
        .create({ fromClient, toClient, payload, nonce });
      let xdata = { [toClient.id]: processor.encrypt() };
      this.chatServer.sendMessage(EXCHANGE_REPLY, fromClient.id, xdata);
      this.chatMediator.set('isLinked', true);
      processor.destroy();
      return toClient;
    },

    sendClientInfoToEveryone() {
      let toClients = this.currentUser.findEveryoneElse()
        .filter(client => client.id !== this.client.id);
      let fromClient = this.currentUser.client;
      let payload = this.client.serialize({ includeId: true });
      let processor = getOwner(this)
        .factoryFor('message-processor:session')
        .create({ fromClient, payload });
      let xdata = {};
      for (let toClient of toClients) {
        xdata[toClient.id] = processor.encrypt(toClient);
      }
      this.chatServer.sendMessage(CLIENT_UPDATE, fromClient.id, xdata);
      processor.destroy();
    },

    linkExpirationTimer: task(function * (exp) {
      let now = new Date().getTime();
      let expTime = new Date(exp).getTime();
      yield timeout(expTime - now);
    }).restartable(),

    *perform () {
      const { chatServer, currentUser } = this;
      currentUser.generateEphemeralKeypair();
      try {
        let from = currentUser.client.id;
        let pubKey = serializeUint8Array(currentUser.ephemeralPublicKey);
        let { link, exp } = yield chatServer.requestLink(from, pubKey);
        this.setProperties({ link, exp });
        let exchange = yield race([
          waitForEvent(chatServer, eventFor(EXCHANGE_REQUEST)),
          this.linkExpirationTimer.perform(exp)
        ]);
        this.linkExpirationTimer.cancelAll();
        this.processExchange(exchange);
        this.sendClientInfoToEveryone();
        sodium.increment(this.authNonce);
        return this.sendExchangeReply();
      } finally {
        currentUser.wipeEphemeralKeys();
      }
    }
  });
}
