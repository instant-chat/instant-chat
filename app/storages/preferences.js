import StorageObject from 'ember-local-storage/local/object';

const Storage = StorageObject.extend();

Storage.reopenClass({
  initialState() {
    return {
      bigTextColor: '#ffffff',
      bigTextBgColor: '#000000'
    };
  }
});

export default Storage;
