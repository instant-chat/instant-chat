import { isBlank } from '@ember/utils';

const MIN_FONT_SIZE = 24;
const MAX_FONT_SIZE = 96;

function propsFor(container) {
  return {
    get(name) { return container.style.getPropertyValue(name); },
    getInt(name) { return parseInt(this.get(name), 10); }
  };
}

function adjustorFor(container, functor) {
  let props = propsFor(container);
  return function(name) {
    let unit = props.get(name).replace(/^\d+/, '');
    let value = functor(props.getInt(name));
    return `${value}${unit}`;
  }
}

export default class FontResizer {
  constructor(wrapper, content) {
    this.wrapper = {
      element: wrapper,
      style: window.getComputedStyle(wrapper)
    };
    this.content = {
      element: content,
      style: window.getComputedStyle(content)
    };
  }

  increaseToFit() {
    let failsafe = 10000;
    let wrapper = propsFor(this.wrapper);
    let content = propsFor(this.content);
    let increment = adjustorFor(this.content, v => v + 1);
    function isUnderUpperBounds() {
      return content.getInt('height') < wrapper.getInt('height');
    }
    function isBelowMaxFontSize() {
      return content.getInt('font-size') < MAX_FONT_SIZE
    }
    while (isUnderUpperBounds() && isBelowMaxFontSize()) {
      if (!--failsafe) throw new Error('Infinate loop');
      this.content.element.style.fontSize = increment('font-size');
    }
  }

  decreaseToFit() {
    let failsafe = 10000;
    let wrapper = propsFor(this.wrapper);
    let content = propsFor(this.content);
    let decrement = adjustorFor(this.content, v => v - 1);
    function fitsWithinBounds() {
      return content.getInt('height') < wrapper.getInt('height');
    }
    function isAboveMinFontSize() {
      return content.getInt('font-size') > MIN_FONT_SIZE;
    }
    while (!fitsWithinBounds() && isAboveMinFontSize()) {
      if (!--failsafe) throw new Error('Infinate loop');
      this.content.element.style.fontSize = decrement('font-size');
    }
  }

  resizeToFit() {
    if (isBlank(this.content.element.innerText)) {
      this.content.element.style.fontSize = `${MAX_FONT_SIZE}px`;
      return;
    }
    this.increaseToFit();
    this.decreaseToFit();
  }
}
