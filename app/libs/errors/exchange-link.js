import { ApplicationError } from './application';

export class ExpiredLinkError extends ApplicationError {
  constructor(link) {
    super(`Link ${link} has expired`);
    this.name = 'ExpiredLinkError';
    this.link = link;
  }
}

export function isExpiredLinkError(error) {
  return error instanceof ExpiredLinkError;
}
