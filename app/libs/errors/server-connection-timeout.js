import { ApplicationError } from './application';

export class ServerConnectionTimeoutError extends ApplicationError {
  constructor(message = 'Handshake with server has timed out') {
    super(message);
    this.name = 'ServerConnectionTimeoutError';
  }
}

export function isServerConnectionTimeoutError(error) {
  return error instanceof ServerConnectionTimeoutError;
}
