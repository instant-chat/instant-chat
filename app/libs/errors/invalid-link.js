import { ApplicationError } from './application';

export class InvalidLinkError extends ApplicationError {
  constructor(link) {
    super(`Link ${link} is an invalid link`);
    this.name = 'InvalidLinkError';
    this.link = link;
  }
}

export function isInvalidLinkError(error) {
  return error instanceof InvalidLinkError;
}
