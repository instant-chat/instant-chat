import { ApplicationError } from './application';

export class SecretExpiredError extends ApplicationError {
  constructor(message = 'The secret has expired and is no longer available.') {
    super(message);
    this.name = 'SecretExpiredError';
  }
}

export function isSecretExpiredError(error) {
  return error instanceof SecretExpiredError;
}
