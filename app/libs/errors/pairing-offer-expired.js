import { ApplicationError } from './application';

export class PairingOfferExpiredError extends ApplicationError {
  constructor(message = 'Pairing offer has expired before establishing an exchange.') {
    super(message);
    this.name = 'PairingOfferExpiredError';
  }
}

export function isPairingOfferExpiredError(error) {
  return error instanceof PairingOfferExpiredError;
}
