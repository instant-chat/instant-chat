export const EXCHANGE_REQUEST = 'exchange-request';
export const EXCHANGE_REPLY = 'exchange-reply';
export const CLIENT_UPDATE = 'client-update';
export const SESSION = 'session';
