import EmberRouter from '@ember/routing/router';
import config from './config/environment';

const Router = EmberRouter.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('chat', function() {
    this.route('link');
    this.route('help');
  });
  this.route('secret', function() {
    this.route('show', { path: '/' });
    this.route('link', { path: '/:id' });
    this.route('list');
    this.route('new');
  });
  this.route('big');
  this.route('about', function() {
    this.route('big');
    this.route('chat');
    this.route('secret');
  });
  this.route('preferences');
  this.route('chat-exchange', { path: '/*link' });
});

export default Router;
