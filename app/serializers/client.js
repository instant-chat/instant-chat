import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  attrs: {
    isSelf: { serialize: false },
    ephemeralKeys: { serialize: false },
    sessionKeys: { serialize: false }
  }
});
