import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { race, task, timeout } from 'ember-concurrency';
import { InvalidLinkError, PairingOfferExpiredError }
  from 'instant-chat/libs/errors';
import acceptPairingSessionTask
  from 'instant-chat/workflows/accept-pair-task';

const CONNECTION_TIMEOUT = 90000;

function isLink(link) {
  return link && /^[A-Z]{4}$/i.test(link);
}

export default Route.extend({
  intl: service(),
  chatMessages: service(),
  currentUser: service(),

  acceptPairingSession: acceptPairingSessionTask(),

  fetchModel: task(function * (link) {
    if (!isLink(link)) { throw new InvalidLinkError(link); }
    let model = yield race([
      timeout(CONNECTION_TIMEOUT),
      this.acceptPairingSession.perform(link.toUpperCase())
    ]);
    if (!model) throw new PairingOfferExpiredError();
    return model;
  })
    .restartable()
    .cancelOn('deactivate'),

  model({ link }) {
    return this.fetchModel.perform(link);
  },

  afterModel() {
    const { META } = this.chatMessages;
    let count = this.currentUser.findEveryoneElse().length;
    let body = this.intl.t('meta_messages.you_have_joined', { count });
    this.chatMessages.createMessage(META, body);
    return this.transitionTo('chat.index');
  }
});
