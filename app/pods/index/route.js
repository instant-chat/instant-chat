import Route from '@ember/routing/route';

export default Route.extend({
  beforeModel() {
    let hasPairedWithOthers = this.store.peekAll('client').length > 1;
    let route = hasPairedWithOthers ? 'chat.index' : 'chat.link';
    return this.transitionTo(route);
  }
});
