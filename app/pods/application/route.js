import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import sodium from 'libsodium-wrappers';
import { timeout } from 'ember-concurrency';
import { race } from 'rsvp';
import { ServerConnectionTimeoutError } from 'instant-chat/libs/errors';

const INITILIZATION_TIMEOUT = 8000;
const CONFIRM_MSG = "Chat history will be lost.";

export default Route.extend({
  intl: service(),
  currentUser: service(),
  chatServer: service(),
  chatMediator: service(),
  chatMessages: service(),

  async setupServerConnections() {
    await this.chatServer.connect();
    await this.chatMediator.connect();
    return true;
  },

  async beforeModel() {
    this.intl.setLocale('en-us');
    await sodium.ready;
    let connected = await race([
      timeout(INITILIZATION_TIMEOUT),
      this.setupServerConnections()
    ]);
    if (!connected) {
      throw new ServerConnectionTimeoutError();
    }
  },

  async model() {
    return this.currentUser.client || await this.currentUser.createSelfClient();
  },

  async afterModel(model) {
    const { INITIAL_JOIN } = this.chatMessages;
    this.currentUser.client = model;
    await this.chatServer.registerClient(model.id);
    this.chatMessages.createMessage(INITIAL_JOIN, model.name);
    await this.autoDestructSecretLinks();
  },

  activate() {
    this.set('boundConfirmUnload', this.confirmUnload.bind(this));
    window.onbeforeunload = this.boundConfirmUnload;
  },

  deactivate() {
    window.onbeforeunload = null;
  },

  async autoDestructSecretLinks() {
    let secretLinks = await this.store.findAll('secret-link');
    secretLinks.forEach(secretLink => secretLink.autoDestruct.perform());
  },

  confirmUnload(event) {
    if (this.chatMediator.isLinked) {
      if (!window.confirm(CONFIRM_MSG)) {
        event.preventDefault();
      }
      return CONFIRM_MSG;
    }
  }
});
