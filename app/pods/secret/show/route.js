import Route from '@ember/routing/route';
import { isBlank } from '@ember/utils';
import { inject as service } from '@ember/service';
import { getOwner } from '@ember/application';
import sodium from 'libsodium-wrappers';
import URI from 'urijs';

export default Route.extend({
  chatServer: service(),

  beforeModel() {
    if (isBlank(new URI().fragment())) {
      return this.transitionTo('secret.new');
    }
  },

  async model() {
    let [id, secretKey = ''] = new URI().fragment().split(':');
    let { payload } = await this.chatServer.fetchSecretForID(id);
    let processor = getOwner(this)
      .factoryFor('message-processor:secret')
      .create({ payload, secretKey: sodium.from_base64(secretKey) });
    let secretText = processor.decrypt();
    processor.destroy();
    return { secretText };
  }
});
