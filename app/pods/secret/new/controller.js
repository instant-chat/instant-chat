import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
    showSecretLink(secretLink) {
      this.transitionToRoute('secret.link', secretLink);
    }
  }
});
