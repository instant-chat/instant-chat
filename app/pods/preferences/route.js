import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';

export default Route.extend({
  currentUser: service(),

  async deactivate() {
    this._super(...arguments);
    await this.currentUser.client.save();
  }
});
