import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { alias } from '@ember/object/computed';
import { storageFor } from 'ember-local-storage';

export default Controller.extend({
  currentUser: service(),
  preferences: storageFor('preferences'),

  currentUserName: alias('currentUser.client.name'),

  actions: {
    updateClientName(name) {
      this.get('currentUser').assignName(name);
      this.set('preferences.clientName', name);
    },

    anonymizeClientName() {
      this.get('currentUser').assignName(null);
      this.set('preferences.clientName', null);
    }
  }
});
