import Component from '@ember/component';
import { computed } from '@ember/object';

export default Component.extend({
  classNames: ['radio-picker'],

  state: computed('value', function() {
    return { [this.value]: true };
  })
});
