import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { getOwner } from '@ember/application';
import { scheduleOnce } from '@ember/runloop';
import { dasherize } from '@ember/string';
import { isBlank } from '@ember/utils';
import { task } from 'ember-concurrency';
import { OK, NOT_OK } from 'instant-chat/libs/symbols';
import config from 'instant-chat/config/environment';

const ENTER_KEY = 'Enter';

export default Component.extend({
  intl: service(),
  chatMessages: service(),

  tagName: '',

  async createChatMessage(body) {
    const { CHAT } = this.chatMessages;
    let message = await this.chatMessages.createMessage(CHAT, body);
    return { result: OK, message };
  },

  async handleUnknownChatCommand(command) {
    const { META } = this.chatMessages;
    let body = this.intl.t('meta_messages.unknown_command', { command });
    let message = await this.chatMessages.createMessage(META, body);
    return { result: NOT_OK, message };
  },

  async handleChatCommand(input) {
    let [command, body] = input.split(/\s+/, 2);
    let ChatCommand = getOwner(this)
      .factoryFor(`chat-command:${dasherize(command)}`);
    if (!ChatCommand) { return this.handleUnknownChatCommand(command); }
    let chatCommand = ChatCommand.create({ body });
    try {
      return chatCommand.execute();
    } finally {
      chatCommand.destroy();
    }
  },

  attemptInputFocus() {
    if (!this.refocusSelector) return;
    document.querySelector(this.refocusSelector).focus();
  },

  processInput: task(function* () {
    let message = this.value;
    if (isBlank(message)) { return; }
    let { result } = message[0] === '/'
      ? yield this.handleChatCommand(message.substr(1))
      : yield this.createChatMessage(message);
    if (result === OK) {
      this.set('value', '');
    }
    scheduleOnce('afterRender', this, 'attemptInputFocus');
  }).enqueue(),

  actions: {
    autoCompleteHint(hint) {
      let messageArgs = this.value.replace(/^\/\S*\s*/, '');
      let newMessage = `/${hint} ${messageArgs}`;
      this.set('commandHints', null);
      this.set('value', newMessage);
      scheduleOnce('afterRender', this, 'attemptInputFocus');
    },

    handleKey(event) {
      if (event.key !== ENTER_KEY) return;
      event.preventDefault();
      if (event.shiftKey) {
        this.set('value', `${this.value}\n`);
      } else {
        this.processInput.perform();
      }
    },

    updateValueAndCommandHints(message) {
      this.set('value', message);
      let commandHints = null;
      try {
        if (message[0] !== '/' || message.length < 2) { return; }
        let filterTerm = message.substr(1);
        commandHints = config.APP.availableChatCommands
          .filter(command => command.startsWith(filterTerm));
        if (commandHints.length === 1 && commandHints[0] === filterTerm) {
          commandHints = null;
        }
      } finally {
        this.set('commandHints', commandHints);
      }
    }
  }
});
