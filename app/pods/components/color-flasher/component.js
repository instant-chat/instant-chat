import Component from '@ember/component';
import { or } from '@ember/object/computed';
import { task, timeout } from 'ember-concurrency';

export default Component.extend({
  tagName: '',

  yieldedFgColor: or('{tempFgColor,fgColor}'),
  yieldedBgColor: or('{tempBgColor,bgColor}'),

  flashColors: task(function* () {
    let setTempColors = () => {
      this.set('tempFgColor', this.fgColor);
      this.set('tempBgColor', this.bgColor);
    };
    let setInverseTempColors = () => {
      this.set('tempFgColor', this.bgColor);
      this.set('tempBgColor', this.fgColor);
    };
    try {
      while (true) {
        yield timeout(200);
        setInverseTempColors();
        yield timeout(200);
        setTempColors();
      }
    } finally {
      this.set('tempFgColor', null);
      this.set('tempBgColor', null);
    }
  }).restartable()
});
