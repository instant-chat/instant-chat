import Component from '@ember/component';

export default Component.extend({
  classNames: ['loading-indecator', 'loading'],
  classNameBindings: ['small::loading-lg'],
  small: false
});
