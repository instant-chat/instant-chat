import Component from '@ember/component';
import { task, timeout } from 'ember-concurrency';
import distanceInWordsToNow from 'date-fns/distance_in_words_to_now';
import isPast from 'date-fns/is_past';

const ONE_SECOND = 1000;

export default Component.extend({
  tagName: 'time',
  classNames: ['live-relative-time'],
  attributeBindings: 'date:datetime',

  addSuffix: false,
  includeSeconds: true,
  refreshInterval: ONE_SECOND,

  relativeTimeCounter: task(function* () {
    do {
      let options = this.getProperties('addSuffix', 'includeSeconds');
      this.set('relativeTime', distanceInWordsToNow(this.date, options));
      this.set('hasExpired', isPast(this.date));
      yield timeout(this.refreshInterval);
    } while (this.refreshInterval > 0);
  }).on('didInsertElement')
});
