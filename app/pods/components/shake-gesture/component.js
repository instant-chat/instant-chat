import Component from '@ember/component';
import Shake from 'shake.js';

export default Component.extend({
  tagName: '',

  didInsertElement() {
    this._super(...arguments);
    this.shakeDetector = new Shake();
    this.shakeDetector.start();
    window.addEventListener('shake', this, false);
  },

  willDestroyElement() {
    this._super(...arguments);
    this.shakeDetector.stop();
    window.removeEventListener('shake', this, false);
  },

  handleEvent(event) {
    this.onShake(event);
  }
});
