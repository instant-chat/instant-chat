import Component from '@ember/component';

export default Component.extend({
  tagName: 'ul',
  classNames: ['hint-box', 'menu']
});
