import Component from '@ember/component';
import { task, timeout } from 'ember-concurrency';
import { seconds } from 'instant-chat/utils/wait-for-expired'
const { floor } = Math;

const ONE_SECOND = 1000;

export default Component.extend({
  tagName: 'meter',
  classNames: ['live-countdown-meter', 'meter'],
  attributeBindings: ['value', 'min', 'max', 'low', 'high', 'optimum'],

  lowPercentage: 0.1,
  highPercentage: 0.2,
  refreshInterval: ONE_SECOND,

  relativeTimeCounter: task(function* () {
    let spanInSeconds = seconds(this.date) - seconds(new Date());
    this.set('max', 0);
    this.set('max', spanInSeconds);
    this.set('low', floor(spanInSeconds * this.lowPercentage));
    this.set('high', floor(spanInSeconds * this.highPercentage));
    this.set('optimum', spanInSeconds);
    do {
      spanInSeconds = seconds(this.date) - seconds(new Date());
      this.set('value', spanInSeconds);
      yield timeout(this.refreshInterval);
    } while (this.refreshInterval > 0 && spanInSeconds > 0);
  }).on('didInsertElement')
});
