import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { computed } from '@ember/object';
import { htmlSafe, underscore } from '@ember/string';
import { isApplicationError } from 'instant-chat/libs/errors';

export default Component.extend({
  intl: service(),

  classNames: ['error-message', 'toast', 'toast-error'],

  errorMessage: computed('error', function() {
    if (!this.error) { return; }
    let key = isApplicationError(this.error)
      ? underscore(this.error.name)
      : 'runtime_error';
    return htmlSafe(this.intl.t(`errors.${key}`, this.error));
  })
});

