import Component from '@ember/component';
import { assign } from '@ember/polyfills';
import { inject as service } from '@ember/service';
import { task } from 'ember-concurrency';
import saveSecretTask from 'instant-chat/workflows/save-secret-task';

export default Component.extend({
  store: service(),

  tagName: '',

  saveSecret: saveSecretTask(),

  saveNewSecretLink: task(function* (formValues) {
    let resultPayload = yield this.saveSecret.perform(formValues);
    let modelAttrs = assign(resultPayload, {
      createdAt: new Date().toISOString(),
      description: formValues.description
    });
    let secretLink = this.store.createRecord('secret-link', modelAttrs);
    yield secretLink.save();
    secretLink.autoDestruct.perform();
    this.onCreated(secretLink);
  }).drop()
});
