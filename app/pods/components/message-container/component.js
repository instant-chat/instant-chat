import Component from '@ember/component';
import { computed } from '@ember/object';
import { reads } from '@ember/object/computed';
import { inject as service } from '@ember/service';

export default Component.extend({
  unreadMessages: service(),

  classNames: ['message-container'],
  classNameBindings: [
    'isUnread:--unread',
    'isAnonymous:--anonymous',
    'isEvicted:--evicted',
    'isSelf:--from-self:--from-other',
    'typeClass'
  ],

  isAnonymous: reads('message.from.isAnonymous'),
  isEvicted: reads('message.from.isEvicted'),
  isSelf: reads('message.from.isSelf'),

  isUnread: computed('{message,unreadMessages.unreadSet}', function() {
    return this.unreadMessages.includes(this.message);
  }),

  typeClass: computed('message.type', function() {
    return `message-${this.message.type}`;
  }),

  componentName: computed('message.type', function() {
    return `message-container/${this.message.type}`;
  })
});
