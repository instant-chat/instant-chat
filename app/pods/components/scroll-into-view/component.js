import Component from '@ember/component';
import { isElementAtBottomOfPage } from 'instant-chat/utils/viewport';

export default Component.extend({
  tagName: 'span',
  classNames: ['scroll-into-view'],

  didInsertElement() {
    this._super(...arguments);
    if (isElementAtBottomOfPage(this.element)) {
      this.element.scrollIntoView(true);
    }
  }
});
