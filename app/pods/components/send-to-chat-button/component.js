import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { task, timeout } from 'ember-concurrency';
import { translationMacro as t } from 'ember-intl';
import config from 'instant-chat/config/environment';

export default Component.extend({
  intl: service(),
  chatMessages: service(),

  tagName: 'button',
  classNames: ['send-to-chat-button', 'btn'],
  classNameBindings: ['sendToChat.isRunning:tooltip'],
  attributeBindings: ['tooltip:data-tooltip'],

  tooltip: t('sent_to_chat'),

  click() {
    this.sendToChat.perform();
  },

  sendToChat: task(function* () {
    let { LINK } = this.chatMessages;
    yield this.chatMessages.createMessage(LINK, this.value);
    yield timeout(config.APP.tooltipAutoHideDelay);
  }).restartable()
});
