import Component from '@ember/component';
import { empty } from '@ember/object/computed';
import { w } from '@ember/string';
import { task } from 'ember-concurrency';

const ONE_MINUTE = 60;
const FIVE_MINUTES = 5 * ONE_MINUTE;
const ONE_HOUR = 60 * ONE_MINUTE;
const ONE_DAY = 24 * ONE_HOUR;
const ONE_WEEK = 7 * ONE_DAY;
const FORM_PROPERTY_NAMES = w('description secretText ttl');

export const TTL_OPTIONS = Object.freeze([
  { code: FIVE_MINUTES, label: 'ttl.five_minutes' },
  { code: ONE_HOUR, label: 'ttl.an_hour' },
  { code: ONE_DAY, label: 'ttl.a_day' },
  { code: ONE_WEEK, label: 'ttl.a_week' }
]);

export const DEFAULT_TTL = ONE_HOUR;

export default Component.extend({
  ttlOptions: TTL_OPTIONS,
  saveDisabled: empty('secretText'),

  init() {
    this._super(...arguments);
    this.resetToDefaults();
  },

  resetToDefaults() {
    this.set('secretText', null);
    this.set('ttl', DEFAULT_TTL);
  },

  savingTask: task(function* () {
    yield this.save(this.getProperties(...FORM_PROPERTY_NAMES));
    this.resetToDefaults();
  }).drop()
});
