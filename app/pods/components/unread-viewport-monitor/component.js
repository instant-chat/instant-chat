import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { isElementAtBottomOfPage } from 'instant-chat/utils/viewport';

export default Component.extend({
  unreadMessages: service(),

  tagName: 'span',
  classNames: ['unread-viewport-monitor'],

  didInsertElement() {
    this._super(...arguments);
    this.handleEvent();
    window.addEventListener('scroll', this, false);
    window.addEventListener('resize', this, false);
    this.unreadMessages.on('newUnreadMessages', this, this.handleEvent);
  },

  willDestroyElement() {
    this._super(...arguments);
    window.removeEventListener('scroll', this, false);
    window.removeEventListener('resize', this, false);
    this.unreadMessages.off('newUnreadMessages', this, this.handleEvent);
  },

  handleEvent() {
    if (isElementAtBottomOfPage(this.element)) {
      this.unreadMessages.markAllRead();
    }
  }
});
