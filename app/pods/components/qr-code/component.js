import Component from '@ember/component';
import { task } from 'ember-concurrency';
import { reads } from '@ember/object/computed';
import QRCode from 'qrcode';

export default Component.extend({
  tagName: '',

  error: reads('createQrCode.last.error'),
  isLoading: reads('createQrCode.isRunning'),
  qrCodeUri: reads('createQrCode.last.value'),

  createQrCode: task(function* () {
    return yield QRCode.toDataURL(this.data);
  })
    .restartable()
    .observes('data')
    .on('didInsertElement')
});
