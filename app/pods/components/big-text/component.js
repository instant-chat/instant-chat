import Component from '@ember/component';
import { computed } from '@ember/object';
import { htmlSafe } from '@ember/string';
import { scheduleOnce } from '@ember/runloop';
import FontResizer from 'instant-chat/libs/font-resizer';

export default Component.extend({
  classNames: ['big-text'],
  attributeBindings: ['style'],

  style: computed('{fgColor,bgColor}', function() {
    let fgColor = this.get('fgColor');
    let bgColor = this.get('bgColor');
    return htmlSafe(`color: ${fgColor}; background-color: ${bgColor};`);
  }),

  didInsertElement() {
    this._super(...arguments);
    this.fontResizer = new FontResizer(
      this.element.querySelector('.big-text__wrapper'),
      this.element.querySelector('.big-text__content')
    );
    this.fontResizer.resizeToFit();
  },

  setFocus(giveFocus) {
    let el = this.element.querySelector('.big-text__content');
    let method = giveFocus ? 'focus' : 'blur';
    el[method]();
  },

  actions: {
    clear() {
      this.set('text', '');
      this.setFocus(true);
      this.send('resizeWhileTyping');
    },

    resizeWhileTyping() {
      scheduleOnce('afterRender', this.fontResizer, 'resizeToFit');
    }
  }
});
