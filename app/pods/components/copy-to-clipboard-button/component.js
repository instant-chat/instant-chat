import Component from '@ember/component';
import { inject as service } from '@ember/service';
import { task, timeout } from 'ember-concurrency';
import { translationMacro as t } from 'ember-intl';
import config from 'instant-chat/config/environment';

export default Component.extend({
  intl: service(),

  tagName: 'button',
  classNames: ['copy-to-clipbord-button', 'btn'],
  classNameBindings: ['copyText.isRunning:tooltip'],
  attributeBindings: ['tooltip:data-tooltip'],

  tooltip: t('copied'),

  click() {
    this.copyText.perform();
  },

  copyText: task(function* () {
    yield navigator.clipboard.writeText(this.value);
    yield timeout(config.APP.tooltipAutoHideDelay);
  }).restartable()
});
