import Controller from '@ember/controller';
import { filterBy, sort } from '@ember/object/computed';

export default Controller.extend({
  sort: Object.freeze(['createdAt:asc']),
  messages: sort('model.messages', 'sort'),
  clients: filterBy('model.clients', 'isEvicted', false)
});
