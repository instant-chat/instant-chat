import Route from '@ember/routing/route';

export default Route.extend({
  model() {
    return {
      messages: this.store.peekAll('message'),
      clients: this.store.peekAll('client')
    };
  }
});
