import Controller from '@ember/controller';
import config from 'instant-chat/config/environment';

export default Controller.extend({
  allHints: config.APP.availableChatCommands,

  actions: {
    close() {
      this.transitionToRoute('chat.index');
    }
  }
});
