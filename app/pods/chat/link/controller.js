import Controller from '@ember/controller';
import { inject as service } from '@ember/service';
import { not, reads } from '@ember/object/computed';
import { computed } from '@ember/object';
import { storageFor } from 'ember-local-storage';
import URI from 'urijs';

export default Controller.extend({
  intl: service(),

  error: reads('model.task.last.error'),
  exp: reads('model.pairTask.lastRunning.exp'),
  isFetchingLink: not('link'),
  link: reads('model.pairTask.lastRunning.link'),
  preferences: storageFor('preferences'),

  linkIntroductionText: computed('preferences.linkIntroductionText', function() {
    return this.get('preferences.linkIntroductionText')
      || this.intl.t('link_introduction_text');
  }),

  href: computed('link', function() {
    return new URI().path(this.link).toString();
  }),

  actions: {
    close() {
      this.transitionToRoute('chat.index');
    }
  }
});
