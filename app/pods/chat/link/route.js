import Route from '@ember/routing/route';
import { inject as service } from '@ember/service';
import { task } from 'ember-concurrency';
import { isPairingOfferExpiredError } from 'instant-chat/libs/errors';
import offerPairingSessionTask from 'instant-chat/workflows/offer-pair-task';

export default Route.extend({
  intl: service(),
  chatMessages: service(),

  offerPairingSession: offerPairingSessionTask(),

  fetchModel: task(function* () {
    const { META } = this.chatMessages;
    let client;
    try {
      client = yield this.offerPairingSession.perform();
    } catch(error) {
      return this.handlePairingError(error);
    }
    let name = client.name;
    let tKey = name
      ? 'meta_messages.name_joined'
      : 'meta_messages.someone_joined';
    this.chatMessages.createMessage(META, this.intl.t(tKey, { name }))
    yield this.transitionTo('chat.index');
  }).restartable().cancelOn('deactivate'),

  handlePairingError(error) {
    if (!isPairingOfferExpiredError(error)) { throw error; }
    const { META } = this.chatMessages;
    let body = this.intl.t('meta_messages.pairing_offer_expired');
    this.chatMessages.createMessage(META, body);
    this.transitionTo('chat.index');
  },

  model() {
    let task = this.fetchModel.perform();
    let pairTask = this.offerPairingSession;
    return { task, pairTask };
  }
});
