import DS from 'ember-data';
import { serializeUint8Array, deserializeUint8Array }
  from '../utils/primitive-utils';
const { Transform } = DS;

export default Transform.extend({
  deserialize(serialized) {
    if (!serialized) { return; }
    let { keyType, publicKey, privateKey } = serialized;
    return {
      keyType,
      publicKey: publicKey && deserializeUint8Array(publicKey),
      privateKey: privateKey && deserializeUint8Array(privateKey)
    };
  },

  serialize(deserialized, options = {}) {
    if (!deserialized) { return; }
    let { keyType, publicKey, privateKey } = deserialized;
    let json = {
      keyType,
      publicKey: publicKey && serializeUint8Array(publicKey),
      privateKey: privateKey && serializeUint8Array(privateKey)
    };
    if (!options.includePrivateKey) {
      delete json.privateKey;
    }
    return json;
  }
});
