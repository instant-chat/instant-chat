import EmberObject from '@ember/object';
import { inject as service } from '@ember/service';
import { OK } from 'instant-chat/libs/symbols';

export default EmberObject.extend({
  router: service(),

  execute() {
    this.router.transitionTo('chat.link');
    return { result: OK };
  }
});
