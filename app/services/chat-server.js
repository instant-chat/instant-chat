import Service, { inject as service } from '@ember/service';
import Evented from '@ember/object/evented';
import { runInDebug } from '@ember/debug';
import { isGoneError } from 'ember-ajax/errors';
import { waitForEvent } from 'ember-concurrency';
import { SecretExpiredError } from 'instant-chat/libs/errors';

export default Service.extend(Evented, {
  ajax: service(),
  socketIOService: service('socket-io'),

  log(...args) {
    // eslint-disable-next-line no-console
    runInDebug(() => console.log(...args));
  },

  trigger() {
    this.log('<=', ...arguments);
    this._super(...arguments);
  },

  connect() {
    this.socket = this.socketIOService.socketFor('/');
    this.socket.on('address-assignment', this.onAddressAssignment, this);
    this.socket.on('eviction', this.onEviction, this);
    this.socket.on('message', this.onMessage, this);
    this.socket.on('disconnect', () => this.trigger('disconnect'));
    this.socket.on('reconnect', () => this.trigger('reconnect'));
    return waitForEvent(this, 'room-assigned');
  },

  onAddressAssignment({ address }) {
    let room = this.room || address;
    this.set('address', address);
    this.set('room', room);
    if (address !== room) {
      this.joinRoom(room);
    }
    this.trigger('room-assigned', room);
  },

  onEviction({ client: clientID }) {
    this.trigger('eviction', clientID);
  },

  onMessage({ type, from, xdata }) {
    this.trigger(`message:${type}`, { xdata, from });
  },

  sendMessage(type, from, xdata) {
    this.socket.emit('message', { type, from, xdata });
    this.log('=>', `message:${type}`, { type, from, xdata });
  },

  joinRoom(room) {
    this.set('room', room);
    this.socket.emit('join', { room });
    this.log('=>', 'join', { room });
  },

  registerClient(clientID) {
    this.socket.emit('register-client', { client: clientID });
    this.log('=>', 'register-client', { client: clientID });
  },

  async requestLink(from, pubKey) {
    let room = this.room;
    let data = { room, from, pubKey };
    this.log('=>', 'POST /clients', data);
    let { link, exp } = await this.ajax.post('/clients', { data });
    this.log('<=', '201', link);
    return { link, exp };
  },

  async fetchInfoForLink(link) {
    this.log('=>', `GET /clients/${link}`);
    let response = await this.ajax.request(`/clients/${link}`);
    this.log('<=', '200', response);
    return response;
  },

  async saveSecret({ uuid, ttl, payload }) {
    let data = { uuid, ttl, payload };
    this.log('=>', 'POST /secrets', data);
    let response = await this.ajax.post('/secrets', { data });
    this.log('<=', '201', response);
    return response;
  },

  async fetchSecretForID(uuid) {
    this.log('=>', `GET /secrets/${uuid}`);
    let response;
    try {
      response = await this.ajax.request(`/secrets/${uuid}`);
    } catch(error) {
      if (isGoneError(error)) { throw new SecretExpiredError(); }
      throw error;
    }
    this.log('<=', '200', response);
    return response;
  }
});
