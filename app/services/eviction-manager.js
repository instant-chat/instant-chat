import Service, { inject as service } from '@ember/service';
import { task, timeout } from 'ember-concurrency';
import { tryInvoke } from '@ember/utils';

export default Service.extend({
  evictClientById(clientID) {
    this._evictions = this._evictions || {};
    return this.evictTask.perform(clientID);
  },

  cancelEvictionById(clientID) {
    let evictionTask = this._evictions[clientID];
    if (!evictionTask) { return; }
    evictionTask.cancel();
    tryInvoke(evictionTask.message, 'destroyRecord');
  },

  evictTask: task({
    chatMessages: service(),
    intl: service(),
    store: service(),

    *perform (clientID) {
      const { META } = this.chatMessages;
      let evictedClient = this.store.peekRecord('client', clientID);
      if (!evictedClient) { return; }
      yield evictedClient
        .evict()
        .save({ adapterOptions: { internalOnly: true } });
      try {
        this.owner._evictions[clientID] = this;
        yield timeout(2000);
        let body = this.intl.t('meta_messages.name_evicted', {
          name: evictedClient.name
        });
        let message = yield this.chatMessages.createMessage(META, body);
        this.set('message', message);
        yield timeout(8000);
        this.set('message', null);
      } finally {
        delete this.owner._evictions[clientID];
      }
    }
  })
});
