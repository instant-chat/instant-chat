import Service, { inject as service } from '@ember/service';
import { getOwner } from '@ember/application';
import { runInDebug, warn } from '@ember/debug';
import { isArray } from '@ember/array';
import MESSAGE_TYPES from '../libs/message-types';

function eventFor(type) { return `message:${type}`; }

export default Service.extend({
  intl: service(),
  store: service(),
  chatServer: service(),
  chatMessages: service(),
  currentUser: service(),
  evictionManager: service(),
  unreadMessages: service(),

  isLinked: false,

  connect() {
    const { CLIENT_UPDATE, SESSION } = MESSAGE_TYPES;
    this._super(...arguments);
    this.chatServer.on(eventFor(SESSION), this, this.onSessionMessage);
    this.chatServer.on(eventFor(CLIENT_UPDATE), this, this.onClientUpdateMessage);
    this.chatServer.on('eviction', this, this.onEviction);
    this.chatServer.on('room-assigned', this, this.onReconnected);
  },

  findClient(id) {
    let client = this.store.peekRecord('client', id);
    if (!client) {
      warn(`Unknown client ID ${id}`, { id: 'instant-chat.unknown-client' });
    }
    return client;
  },

  onEviction(clientID) {
    if (!this.isLinked) { return; }
    return this.evictionManager.evictClientById(clientID);
  },

  onReconnected() {
    if (!this.isLinked) { return; }
    this.chatServer.registerClient(this.currentUser.id);
    this.currentUser.client.save();
  },

  onSessionMessage({ xdata, from }) {
    if (!this.isLinked) { return; }
    let payload = xdata[this.currentUser.id];
    let fromClient = this.findClient(from);
    if (!(fromClient && payload)) { return; }
    let processor = getOwner(this)
      .factoryFor('message-processor:session')
      .create({ fromClient, payload });
    let json = processor.decrypt();
    this.store.pushPayload(json);
    // eslint-disable-next-line no-console
    runInDebug(() => console.log('pushPayload', json));
    processor.destroy();
    this.unreadMessages.markPayload(json);
    return json;
  },

  onClientUpdateMessage(payload) {
    const { META } = this.chatMessages;
    if (!this.isLinked) { return; }
    let json = this.onSessionMessage(payload);
    this.cancelEvictions(json);
    let newClients = this.currentUser.generateMissingSessionKeys();
    if (newClients.length === 0) { return; }
    let names = newClients.mapBy('name').join(', ');
    let body = this.intl.t('meta_messages.count_people_have_joined', { names, count: newClients.count });
    this.chatMessages.createMessage(META, body);
  },

  cancelEvictions({ data }) {
    let clientPayloads = isArray(data) ? data : [data];
    for (let clientPayload of clientPayloads) {
      this.evictionManager.cancelEvictionById(clientPayload.id);
    }
  },

  broadcastPayload(messageType, payload) {
    let fromClient = this.currentUser.client;
    let processor = getOwner(this)
      .factoryFor('message-processor:session')
      .create({ fromClient, payload });
    let toClients = this.currentUser.findAllActiveClients();
    let xdata = {};
    for (let toClient of toClients) {
      xdata[toClient.id] = processor.encrypt(toClient);
    }
    processor.destroy();
    return this.chatServer.sendMessage(messageType, fromClient.id, xdata);
  },

  broadcastModels(messageType, models) {
    let data = models.map(model => model.serialize({ includeId: true }).data);
    return this.broadcastPayload(messageType, { data });
  },

  broadcastModel(messageType, model) {
    let data = model.serialize({ includeId: true });
    return this.broadcastPayload(messageType, data);
  },

  broadcastSessionPayload(payload) {
    return this.broadcastPayload(MESSAGE_TYPES.SESSION, payload);
  },

  broadcastSessionModels(models) {
    return this.broadcastModels(MESSAGE_TYPES.SESSION, models);
  },

  broadcastSessionMessage(message) {
    return this.broadcastModel(MESSAGE_TYPES.SESSION, message);
  }
});
