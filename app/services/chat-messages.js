import Service, { inject as service } from '@ember/service';

export const MESSAGE_TYPES = Object.freeze({
  CHAT: 'chat',
  META: 'meta',
  LINK: 'link',
  INITIAL_JOIN: 'initial-join',
  NAME_CHANGE: 'name-change'
});

export const INTERNAL_ONLY_TYPES = Object.freeze([
  MESSAGE_TYPES.META,
  MESSAGE_TYPES.INITIAL_JOIN
]);

export default Service.extend({
  currentUser: service(),
  store: service(),

  async createMessage(
    type,
    body,
    { internalOnly = INTERNAL_ONLY_TYPES.includes(type) } = {}
  ) {
    let createdAt = new Date().toISOString();
    let from = this.currentUser.client;
    let message = this.store.createRecord('message', { type, body, createdAt, from });
    let adapterOptions = { internalOnly };
    await message.save({ adapterOptions });
    return message;
  }
}).reopen(MESSAGE_TYPES);
