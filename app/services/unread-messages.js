import Service from '@ember/service';
import Evented from '@ember/object/evented';
import { equal, reads } from '@ember/object/computed';
import { isArray } from '@ember/array';

export default Service.extend(Evented, {
  allRead: equal('count', 0),
  count: reads('unreadSet.size'),

  init() {
    this._super(...arguments);
    this.set('unreadSet', new Set());
  },

  markUnread(...ids) {
    for (let id of ids) {
      this.unreadSet.add(id);
    }
    this.notifyPropertyChange('unreadSet');
    this.trigger('newUnreadMessages');
    return this;
  },

  markRead(...ids) {
    for (let id of ids) {
      this.unreadSet.delete(id);
    }
    this.notifyPropertyChange('unreadSet');
    return this;
  },

  markAllRead() {
    this.unreadSet.clear();
    this.notifyPropertyChange('unreadSet');
    return this;
  },

  includes(id) {
    return this.unreadSet.has(id);
  },

  markPayload({ data }) {
    if (isArray(data)) {
      this.markUnread(...data.filterBy('type', 'message').mapBy('id'));
    } else if (data.type === 'message') {
      this.markUnread(data.id);
    }
  }
});
