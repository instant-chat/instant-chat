import Service, { inject as service } from '@ember/service';
import { reads } from '@ember/object/computed';
import { storageFor } from 'ember-local-storage';
import fakeName from '../utils/fake-name';
import {
  generateEphemeralKeypair,
  generateExchangeKeypair,
  generateClientSessionKeys,
  generateServerSessionKeys
} from '../utils/crypto-utils';
import { isNone } from '@ember/utils';

export default Service.extend({
  preferences: storageFor('preferences'),
  intl: service(),
  store: service(),

  id: reads('client.id'),
  name: reads('client.name'),
  ephemeralPublicKey: reads('client.ephemeralKeys.publicKey'),
  exchangeKeys: reads('client.exchangeKeys'),
  sessionKeys: reads('client.sessionKeys'),

  assignName(name) {
    if (name) {
      this.set('client.name', name);
      this.set('client.isAnonymous', false);
    } else {
      let anonName = this.intl.t('anon_user_name', { name: fakeName() });
      this.set('client.name', anonName);
      this.set('client.isAnonymous', true);
    }
    return this;
  },

  findEveryoneElse() {
    return this.store.peekAll('client')
      .filter(client => client.id !== this.id);
  },

  findAllActiveClients() {
    return this.findEveryoneElse().filterBy('isEvicted', false);
  },

  generateMissingSessionKeys(options = {}) {
    let generateSessionKeys = options.isServer
      ? generateServerSessionKeys
      : generateClientSessionKeys;
    let myExchangeKeys = this.exchangeKeys;
    let clients = this.findAllActiveClients();
    let newClients = [];
    for (let client of clients) {
      if (client.isEvicted || client.sessionKeys) { continue; }
      let theirExchangeKeys = client.exchangeKeys;
      let sessionKeys = generateSessionKeys(myExchangeKeys, theirExchangeKeys);
      client.set('sessionKeys', sessionKeys);
      newClients.push(client);
    }
    return newClients;
  },

  generateEphemeralKeypair() {
    let ephemeralKeys = generateEphemeralKeypair();
    this.client.set('ephemeralKeys', ephemeralKeys);
    return ephemeralKeys;
  },

  generateExchangeKeypair() {
    let exchangeKeys = generateExchangeKeypair();
    this.client.set('exchangeKeys', exchangeKeys);
    return exchangeKeys;
  },

  generateServerSessionKeys(client) {
    let sessionKeys = generateServerSessionKeys(
      this.exchangeKeys,
      client.exchangeKeys
    );
    client.set('sessionKeys', sessionKeys);
    return sessionKeys;
  },

  generateClientSessionKeys(client) {
    let sessionKeys = generateClientSessionKeys(
      this.exchangeKeys,
      client.exchangeKeys
    );
    client.set('sessionKeys', sessionKeys);
    return sessionKeys;
  },

  createSelfClient() {
    let client = this.store.createRecord('client', {
      name: this.get('preferences.clientName')
        || this.intl.t('anon_user_name', { name: fakeName() }),
      isAnonymous: isNone(this.get('preferences.clientName')),
      exchangeKeys: generateExchangeKeypair(),
      isSelf: true
    });
    let adapterOptions = { internalOnly: true };
    return client.save({ adapterOptions });
  },

  wipeEphemeralKeys() {
    return this.client.wipeEphemeralKeys();
  }
});
