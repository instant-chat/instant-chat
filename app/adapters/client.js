import ApplicationAdapter from './application';
import { UniqueHashGenerator } from '../utils/primitive-utils';
import { CLIENT_UPDATE } from '../libs/message-types';

export default ApplicationAdapter.extend({
  messageType: CLIENT_UPDATE,

  generateIdForRecord(_, __, inputProperties) {
    return new UniqueHashGenerator()
      .update(inputProperties.exchangeKeys.publicKey)
      .toHash();
  }
});
