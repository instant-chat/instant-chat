import Adapter from 'ember-data/adapter';
import { inject as service } from '@ember/service';
import { SESSION } from '../libs/message-types';

export default Adapter.extend({
  chatMediator: service(),

  messageType: SESSION,

  broadcastChanges(json, options = {}) {
    if (options.internalOnly) { return; }
    this.chatMediator.broadcastPayload(this.messageType, json);
  },

  createRecord(_, __, snapshot) {
    let json = this.serialize(snapshot, { includeId: true });
    this.broadcastChanges(json, snapshot.adapterOptions);
  },

  updateRecord(_, __, snapshot) {
    let json = this.serialize(snapshot, { includeId: true });
    this.broadcastChanges(json, snapshot.adapterOptions);
  },

  deleteRecord() {},

  findRecord(store, type, id) {
    return store.peekRecord(type, id);
  },

  findAll(store, type) {
    return store.peekAll(type);
  }
});
