import ApplicationAdapter from './application';
import { inject as service } from '@ember/service';
import { UniqueHashGenerator } from 'instant-chat/utils/primitive-utils';

export default ApplicationAdapter.extend({
  unreadMessages: service(),

  generateIdForRecord(_, __, inputProperties) {
    return new UniqueHashGenerator()
      .update(inputProperties.type)
      .update(inputProperties.body)
      .update(inputProperties.createdAt)
      .toHash();
  },

  createRecord(_, __, snapshot) {
    this.unreadMessages.markUnread(snapshot.id);
    return this._super(...arguments);
  },

  deleteRecord(_, __, snapshot) {
    this.unreadMessages.markRead(snapshot.id);
    return this._super(...arguments);
  }
});
