import Ember from 'ember';
import { isApplicationError } from 'instant-chat/libs/errors';

export function initialize() {
  Ember.onerror = function errorHandler(error) {
    if (isApplicationError(error)) { return; }
    // eslint-disable-next-line no-console
    console.error(error);
  }
}

export default {
  name: 'error-handler',
  initialize
};
