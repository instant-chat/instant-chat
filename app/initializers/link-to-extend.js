import LinkComponent from '@ember/routing/link-component';

export function initialize() {
  LinkComponent.reopen({
    attributeBindings: ['data-badge']
  });
}

export default {
  name: 'link-to-extend',
  initialize
};
