import sodium from 'libsodium-wrappers';

export function generateEphemeralKeypair() {
  return sodium.crypto_box_keypair();
}

export function generateExchangeKeypair() {
  return sodium.crypto_kx_keypair();
}

export function generateServerSessionKeys(
  { publicKey: serverPublicKey, privateKey: serverPrivateKey },
  { publicKey: clientPublicKey }
) {
  return sodium.crypto_kx_server_session_keys(
    serverPublicKey,
    serverPrivateKey,
    clientPublicKey
  );
}

export function generateClientSessionKeys(
  { publicKey: clientPublicKey, privateKey: clientPrivateKey },
  { publicKey: serverPublicKey }
) {
  return sodium.crypto_kx_client_session_keys(
    clientPublicKey,
    clientPrivateKey,
    serverPublicKey
  );
}
