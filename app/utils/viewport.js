export function isElementAtBottomOfPage(el) {
  let rect = el.getBoundingClientRect();
  let bottom = window.innerHeight || document.documentElement.clientHeight;
  let upperLimit = bottom + rect.height;
  return rect.top <= upperLimit;
}
