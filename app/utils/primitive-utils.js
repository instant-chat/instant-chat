import sodium from 'libsodium-wrappers';
import { assert } from '@ember/debug';
import { typeOf } from '@ember/utils';

export const DEFAULT_HASH_SIZE = 24;

function isUint8ArrayInstance(value) {
  return value instanceof Uint8Array;
}

function isSerializedUint8Array(value) {
  return typeOf(value) === 'string'
    && value.indexOf('uint8:') === 0;
}

export function isUint8Array(value) {
  return isSerializedUint8Array(value)
    || isUint8ArrayInstance(value);
}

export function serializeUint8Array(value) {
  assert(`'${value}' is not a Uint8Array`, isUint8ArrayInstance(value));
  return `uint8:${sodium.to_base64(value)}`;
}

export function deserializeUint8Array(value) {
  assert(`'${value}' is not a serialized Uint8Array`, isSerializedUint8Array(value));
  return sodium.from_base64(value.substring(6));
}

export function concatUint8Array(a, b) {
  let result = new Uint8Array(a.length + b.length);
  result.set(a, 0);
  result.set(b, a.length);
  return result;
}

export function splitUint8Array(a, offset) {
  return [a.slice(0, offset), a.slice(offset)];
}

export class UniqueHashGenerator {
  constructor(hashSize = DEFAULT_HASH_SIZE) {
    let key = sodium.randombytes_buf(sodium.crypto_generichash_KEYBYTES);
    this.hashSize = hashSize;
    this.hashState = sodium.crypto_generichash_init(key, this.hashSize);
  }
  update(value) {
    sodium.crypto_generichash_update(this.hashState, `${value}`);
    return this;
  }
  toHash(options = {}) {
    let hash = sodium.crypto_generichash_final(this.hashState, this.hashSize);
    return options.base64 ? sodium.to_base64(hash) : sodium.to_hex(hash);
  }
}
