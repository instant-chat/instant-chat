import getTime from 'date-fns/get_time';
import { timeout } from 'ember-concurrency';
const { floor, max } = Math;

const ONE_SECOND = 1000;

export function seconds(date) {
  return floor(getTime(date) / ONE_SECOND);
}

export default function(expiration) {
  let delaySeconds = seconds(expiration) - seconds(new Date());
  let delay = delaySeconds * ONE_SECOND;
  return timeout(max(0, delay));
}
