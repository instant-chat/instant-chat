import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { computed } from '@ember/object';
import { task } from 'ember-concurrency';
import isPast from 'date-fns/is_past';
import URI from 'urijs';
import waitForExired from 'instant-chat/utils/wait-for-expired';

export default Model.extend({
  createdAt: attr('string'),
  description: attr('string'),
  exp: attr('string'),
  secretKey: attr('string'),
  uuid: attr('string'),

  hasExpired: computed('exp', function() {
    return isPast(this.exp);
  }).volatile(),

  href: computed('{uuid,secretKey}', function() {
    return new URI()
      .path('/secret')
      .fragment(`${this.uuid}:${this.secretKey}`)
      .toString();
  }),

  autoDestruct: task(function* () {
    yield waitForExired(this.exp);
    this.notifyPropertyChange('hasExpired');
    yield this.destroyRecord();
  }).drop()
});
