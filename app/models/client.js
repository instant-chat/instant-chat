import Model from 'ember-data/model';
import attr from 'ember-data/attr';
import { w } from '@ember/string';
import sodium from 'libsodium-wrappers';

export default Model.extend({
  name: attr('string'),
  isAnonymous: attr('boolean'),
  isEvicted: attr('boolean', { defaultValue: false }),
  isSelf: attr('boolean', { defaultValue: false }),
  exchangeKeys: attr('keypair'),
  ephemeralKeys: attr(),
  sessionKeys: attr(),

  evict() {
    this.set('isEvicted', true);
    return this;
  },

  wipeEphemeralKeys() {
    return this.wipeKeys(w(`
      ephemeralKeys.publicKey
      ephemeralKeys.privateKey
    `.trim()));
  },

  wipeAllKeys() {
    return this.wipeKeys(w(`
      ephemeralKeys.publicKey
      ephemeralKeys.privateKey
      exchangeKeys.publicKey
      exchangeKeys.privateKey
      sessionKeys.sharedTx
      sessionKeys.sharedRx
    `.trim()));
  },

  wipeKeys(props) {
    props
      .map(prop => this.get(prop))
      .forEach(buf => buf && sodium.memzero(buf));
    return this;
  },

  willDestroy() {
    this._super(...arguments);
    this.wipeAllKeys();
  }
});
