import Helper from '@ember/component/helper';
import { get } from '@ember/object';
import config from 'instant-chat/config/environment';

export function getConfig([path]) {
  return get(config, path);
}

export default Helper.helper(getConfig);
