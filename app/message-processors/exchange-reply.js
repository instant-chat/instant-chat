import EmberObject from '@ember/object';
import sodium from 'libsodium-wrappers';
import config from '../config/environment';
import { serializeUint8Array, deserializeUint8Array }
 from '../utils/primitive-utils';
import { runInDebug } from '@ember/debug';

export default EmberObject.extend({
  toClient: null,
  fromClient: null,
  payload: null,
  nonce: null,
  pubKey: null,

  decrypt(ephemeralPublicKey) {
    let result;
    if (config.APP.disableEncryption) {
      result = JSON.parse(this.payload);
    } else {
      let exchangePrivateKey = this.toClient.exchangeKeys.privateKey;
      let ciphertext = deserializeUint8Array(this.payload);
      let plaintext = sodium.crypto_box_open_easy(
        ciphertext,
        this.nonce,
        ephemeralPublicKey,
        exchangePrivateKey
      );
      result = JSON.parse(sodium.to_string(plaintext));
    }
    // eslint-disable-next-line no-console
    runInDebug(() => console.log('\u{1F513} exchange-reply', result));
    return result;
  },

  encrypt() {
    let result;
    if (config.APP.disableEncryption) {
      result = JSON.stringify(this.payload);
    } else {
      let ephemeralPrivateKey = this.fromClient.ephemeralKeys.privateKey;
      let exchangePublicKey = this.toClient.exchangeKeys.publicKey;
      let plaintext = JSON.stringify(this.payload);
      let ciphertext = sodium.crypto_box_easy(
        plaintext,
        this.nonce,
        exchangePublicKey,
        ephemeralPrivateKey
      );
      result = serializeUint8Array(ciphertext);
    }
    // eslint-disable-next-line no-console
    runInDebug(() => console.log('\u{1F510} exchange-reply', result));
    return result;
  }
});
