import EmberObject, { computed } from '@ember/object';
import sodium from 'libsodium-wrappers';
import config from '../config/environment';
import {
  concatUint8Array,
  deserializeUint8Array,
  serializeUint8Array,
  splitUint8Array
} from '../utils/primitive-utils';
import { runInDebug } from '@ember/debug';

export default EmberObject.extend({
  fromClient: null,
  payload: null,

  nonce: computed(function() {
    return sodium.randombytes_buf(sodium.crypto_secretbox_NONCEBYTES);
  }),

  decrypt() {
    let result;
    if (config.APP.disableEncryption) {
      result = JSON.parse(this.payload);
    } else {
      let payload = deserializeUint8Array(this.payload);
      let [nonce, ciphertext]
        = splitUint8Array(payload, sodium.crypto_secretbox_NONCEBYTES);
      let plaintext = sodium.crypto_secretbox_open_easy(
        ciphertext,
        nonce,
        this.fromClient.sessionKeys.sharedRx
      );
      result = JSON.parse(sodium.to_string(plaintext));
    }
    // eslint-disable-next-line no-console
    runInDebug(() => console.log('\u{1F513} session', result));
    return result;
  },

  encrypt(toClient) {
    let result;
    if (config.APP.disableEncryption) {
      result = JSON.stringify(this.payload);
    } else {
      let plaintext = JSON.stringify(this.payload);
      let ciphertext = sodium.crypto_secretbox_easy(
        plaintext,
        this.nonce,
        toClient.sessionKeys.sharedTx
      );
      result = serializeUint8Array(concatUint8Array(this.nonce, ciphertext));
    }
    // eslint-disable-next-line no-console
    runInDebug(() => console.log('\u{1F510} session', result));
    return result;
  }
});
