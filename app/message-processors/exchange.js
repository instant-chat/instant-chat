import EmberObject, { computed } from '@ember/object';
import sodium from 'libsodium-wrappers';
import config from '../config/environment';
import { serializeUint8Array, deserializeUint8Array }
 from '../utils/primitive-utils';
import { runInDebug } from '@ember/debug';

export default EmberObject.extend({
  toClient: null,
  pubKey: null,
  payload: null,

  authNonce: computed(function() {
    return sodium.randombytes_buf(sodium.crypto_box_NONCEBYTES);
  }),

  decrypt() {
    let result;
    if (config.APP.disableEncryption) {
      result = JSON.parse(this.payload);
    } else {
      let { publicKey, privateKey } = this.toClient.ephemeralKeys;
      let ciphertext = deserializeUint8Array(this.payload);
      let plaintext = sodium.crypto_box_seal_open(ciphertext, publicKey, privateKey);
      result = JSON.parse(sodium.to_string(plaintext));
    }
    // eslint-disable-next-line no-console
    runInDebug(() => console.log('\u{1F513} exchange', result));
    return result;
  },

  encrypt() {
    let result;
    if (config.APP.disableEncryption) {
      result = JSON.stringify(this.payload);
    } else {
      let plaintext = JSON.stringify(this.payload);
      let ciphertext = sodium.crypto_box_seal(plaintext, this.pubKey);
      result = serializeUint8Array(ciphertext);
    }
    // eslint-disable-next-line no-console
    runInDebug(() => console.log('\u{1F510} exchange', result));
    return result;
  }
});
