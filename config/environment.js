'use strict';

const fs = require('fs');
const path = require('path');

function buildChatCommandList() {
  let directory = path.join(__dirname, '../app/chat-commands');
  let files = fs.readdirSync(directory);
  return files.map(file => path.basename(file, '.js'));
}

module.exports = function(environment) {
  let ENV = {
    modulePrefix: 'instant-chat',
    podModulePrefix: 'instant-chat/pods',
    environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    'ember-websockets': {
      socketIO: true
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
      availableChatCommands: buildChatCommandList(),
      disableEncryption: false,
      tooltipAutoHideDelay: 2000,
      technicalDocumentationHref: 'https://sandbox.tritarget.org/instant-chat-docs/'
    }
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
    ENV.APP.autoboot = false;
    ENV.APP.tooltipAutoHideDelay = 0;
  }

  if (environment === 'production') {
    // here you can enable a production-specific feature
    ENV.APP.disableEncryption = false;
  }

  return ENV;
};
