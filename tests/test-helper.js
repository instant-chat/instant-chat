import Application from '../app';
import config from '../config/environment';
import { setApplication } from '@ember/test-helpers';
import { start } from 'ember-qunit';
import 'ember-sinon-qunit/test-support/test';

setApplication(Application.create(config.APP));

start();
