import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import Service from '@ember/service';
import { next } from '@ember/runloop';
import { assert } from '@ember/debug';
import { waitForEvent } from 'ember-concurrency';
import sinon from 'sinon';

class MockSocket {
  constructor() { this._listeners = {}; }
  on(evt, fn, ctx) { this._listeners[evt] = [ctx, fn]; }
  off(evt) { this._listeners[evt] = null; }
  one() { assert('Socket.IO events do not implement one() method'); }
  emit(evt, ...args) {
    let [ctx, fn] = this._listeners[evt] || [null, () => {}];
    fn.call(ctx, ...args);
  }
  _waitForEvent(evt) {
    return new Promise(resolve => {
      this.on(evt, payload => {
        this.off(evt);
        resolve(payload);
      });
    });
  }
}

module('Unit | Service | chat-server', function(hooks) {
  setupTest(hooks);

  hooks.beforeEach(function(assert) {
    assert.timeout(100);
    let mockSocket = new MockSocket();
    this.socket = mockSocket;
    this.ajaxSpies = {
      request: sinon.stub(),
      post: sinon.stub()
    };
    this.owner.register('service:ajax', Service.extend(this.ajaxSpies));
    this.owner.register('service:socket-io', Service.extend({
      socketFor() { return mockSocket; }
    }));
    this.initService = async function(service, address = 'foobar') {
      service.connect();
      next(() => this.socket.emit('address-assignment', { address }));
      return await waitForEvent(service, 'room-assigned');
    }
  });

  module('Address Assignment', function() {
    test('it registers an address', async function(assert) {
      let service = this.owner.lookup('service:chat-server');
      await this.initService(service, 'test-address');
      assert.equal(service.address, 'test-address');
    });
  });

  module('Room Management', function() {
    test('#joinRoom assigns a room', async function(assert) {
      let service = this.owner.lookup('service:chat-server');
      await this.initService(service);
      next(() => service.joinRoom('test-room'));
      let result = await this.socket._waitForEvent('join');
      assert.deepEqual(result, { room: 'test-room' });
      assert.equal(service.room, 'test-room');
    });

    test('re-emits eviction events', async function(assert) {
      let expected = { address: 'test-address' };
      let service = this.owner.lookup('service:chat-server');
      await this.initService(service);
      next(() => this.socket.emit('eviction', expected));
      let actual = await waitForEvent(service, 'eviction');
      assert.deepEqual(actual, expected);
    });
  });

  module('Receiving a message', function() {
    test('triggers a message sub-type event when a message is received', async function(assert) {
      let testPayload = {
        type: 'test-message-type',
        from: 'alice',
        xdata: { bob: 'foobar' }
      };
      let expected = { from: 'alice', payload: 'foobar' };
      let service = this.owner.lookup('service:chat-server');
      await this.initService(service);
      service.address = 'bob';
      next(() => this.socket.emit('message', testPayload));
      let actual = await waitForEvent(service, 'message:test-message-type');
      assert.deepEqual(actual, expected);
    });
  });

  module('Sending a message', function() {
    test('sends a message with from address', async function(assert) {
      let testPayload = { bob: 'foobar' };
      let expected = {
        type: 'test-message-type',
        from: 'alice',
        xdata: testPayload
      };
      let service = this.owner.lookup('service:chat-server');
      await this.initService(service);
      service.address = 'alice';
      next(() => service.sendMessage('test-message-type', testPayload));
      let actual = await this.socket._waitForEvent('message');
      assert.deepEqual(actual, expected);
    });
  });

  module('Link exchange', function() {
    test('#requestLink sends room and from to server', async function(assert) {
      let expected = { link: 'test', exp: '2018-12-20T23:02:05.058Z' };
      let service = this.owner.lookup('service:chat-server');
      service.address = 'test-address';
      service.room = 'test-room';
      this.ajaxSpies.post.resolves(expected);
      let actual = await service.requestLink('bada55');
      assert.deepEqual(actual, expected);
      sinon.assert.calledWith(
        this.ajaxSpies.post,
        sinon.match.string,
        { data: { room: 'test-room', from: 'test-address', pubKey: 'bada55' } }
      );
    });

    test('#fetchInfoForLink receives a valid payload', async function(assert) {
      let expected = { room: 'test-room', from: 'test-address', pubKey: 'bada55' };
      let service = this.owner.lookup('service:chat-server');
      this.ajaxSpies.request.resolves(expected);
      let actual = await service.fetchInfoForLink('test');
      assert.deepEqual(actual, expected);
      sinon.assert.calledWith(this.ajaxSpies.request, sinon.match(/\/test$/));
    });
  });

  module('Secret exchange', function() {
    test('#saveSecret sends valid data to server', async function(assert) {
      let testPayload = { uuid: 'test-uuid', ttl: 123, payload: 'bada55' };
      let expected = { exp: 'test-date' };
      let service = this.owner.lookup('service:chat-server');
      this.ajaxSpies.post.resolves(expected);
      let actual = await service.saveSecret(testPayload);
      assert.deepEqual(actual, expected);
      sinon.assert.calledWith(
        this.ajaxSpies.post,
        sinon.match.string,
        { data: testPayload }
      );
    });

    test('#fetchSecretForID receives a valid payload', async function(assert) {
      let expected = { payload: 'bada55' };
      let service = this.owner.lookup('service:chat-server');
      this.ajaxSpies.request.resolves(expected);
      let actual = await service.fetchSecretForID('test-uuid');
      assert.deepEqual(actual, expected);
      sinon.assert.calledWith(this.ajaxSpies.request, sinon.match(/\/test-uuid$/));
    });
  });
});
