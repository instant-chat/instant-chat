import { module, test } from 'qunit';
import { setupTest } from 'ember-qunit';
import sodium from 'libsodium-wrappers';
import sinon from 'sinon';
import { serializeUint8Array } from 'instant-chat/utils/primitive-utils';

module('Unit | Serializer | client', function(hooks) {
  setupTest(hooks);

  hooks.before(async function() {
    await sodium.ready;
  });

  hooks.beforeEach(function() {
    this.store = this.owner.lookup('service:store');
    this.aliceExchangePubKey = sodium.crypto_kx_keypair().publicKey;
    this.bobExchangeKeys = sodium.crypto_kx_keypair();
    this.bobClient = this.store.createRecord('client', {
      id: '1',
      name: 'Bob',
      ephemeralKeys: sodium.crypto_box_keypair(),
      exchangeKeys: this.bobExchangeKeys,
      sessionKeys: sodium.crypto_kx_server_session_keys(
        this.bobExchangeKeys.publicKey,
        this.bobExchangeKeys.privateKey,
        this.aliceExchangePubKey
      )
    });
  });

  test('it does not serialize ephemeral or session keys', function(assert) {
    let result = this.bobClient.serialize();
    sinon.assert.match(this.bobClient, sinon.match.hasNested('ephemeralKeys.privateKey'))
    sinon.assert.match(this.bobClient, sinon.match.hasNested('exchangeKeys.privateKey'))
    sinon.assert.match(this.bobClient, sinon.match.hasNested('sessionKeys.sharedTx'))
    sinon.assert.match(this.bobClient, sinon.match.hasNested('sessionKeys.sharedRx'))
    sinon.assert.match(result, sinon.match.hasNested('data.attributes'));
    assert.notOk(result.data.attributes['ephemeral-keys']);
    assert.notOk(result.data.attributes['session-keys']);
  });

  test('it serializes only the public exchange key', function(assert) {
    let result = this.bobClient.serialize();
    sinon.assert.match(this.bobClient, sinon.match.hasNested('ephemeralKeys.privateKey'))
    sinon.assert.match(this.bobClient, sinon.match.hasNested('exchangeKeys.privateKey'))
    sinon.assert.match(this.bobClient, sinon.match.hasNested('sessionKeys.sharedTx'))
    sinon.assert.match(this.bobClient, sinon.match.hasNested('sessionKeys.sharedRx'))
    sinon.assert.match(result, sinon.match.hasNested(
      'data.attributes.exchange-keys.publicKey',
      sinon.match.string
    ));
    assert.notOk(result.data.attributes['exchange-keys'].privateKey);
  });

  test('it deserializes exchange keys', function(assert) {
    let publicKey = serializeUint8Array(this.aliceExchangePubKey);
    let json = {
      data: {
        id: '2',
        type: 'clients',
        attributes: {
          name: 'Alice',
          'exchange-keys': { publicKey }
        }
      }
    };
    this.store.pushPayload(json);
    let result = this.store.peekRecord('client', '2');
    sinon.assert.match(result, sinon.match.hasNested('exchangeKeys.publicKey'));
    sinon.assert.match(result.exchangeKeys.publicKey, sinon.match.instanceOf(Uint8Array))
    assert.equal(result.name, 'Alice');
  });
});

