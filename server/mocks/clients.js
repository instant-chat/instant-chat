/* eslint-env node */
'use strict';

const MOCK_LINK = 'LINK';

module.exports = function(app) {
  const express = require('express');
  let clientsRouter = express.Router();
  let timer, linkPayload;

  function scheduleExpireLink() {
    clearTimeout(timer);
    timer = setTimeout(function() { linkPayload = null; }, 300000);
  }

  clientsRouter.post('/', function(req, res) {
    linkPayload = req.body;
    scheduleExpireLink();
    let exp = new Date();
    exp.setSeconds(exp.getSeconds() + 300);
    res.status(201).send({ link: MOCK_LINK, exp: exp.toISOString() });
  });

  clientsRouter.get('/:link', function(req, res) {
    if (linkPayload) {
      res.send(linkPayload);
    } else {
      res.status(410).send('Link expired');
    }
    linkPayload = null;
  });

  app.use('/api/clients', require('body-parser').json());
  app.use('/api/clients', clientsRouter);
};
