/* eslint-env node */
'use strict';

module.exports = function(app) {
  const timers = new Map();
  const redisMock = new Map();
  const express = require('express');
  let secretsRouter = express.Router();

  function scheduleExpireSecret(uuid, ttl) {
    clearTimeout(timers.get(uuid));
    let timer = setTimeout(function() {
      timers.delete(uuid);
      redisMock.delete(uuid);
    }, ttl * 1000);
    timers.set(uuid, timer);
  }

  secretsRouter.post('/', function(req, res) {
    let { uuid, ttl, payload } = req.body;
    let exp = new Date();
    exp.setSeconds(exp.getSeconds() + ttl);
    redisMock.set(uuid, payload);
    scheduleExpireSecret(uuid, ttl);
    res.status(201).send({ exp: exp.toISOString() });
  });

  secretsRouter.get('/:uuid', function(req, res) {
    let { uuid } = req.params;
    let payload = redisMock.get(uuid);
    if (payload) {
      res.send({ payload });
    } else {
      res.status(410).send('Secret destroyed');
    }
  });

  app.use('/api/secrets', require('body-parser').json());
  app.use('/api/secrets', secretsRouter);
};
