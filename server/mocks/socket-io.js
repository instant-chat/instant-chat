/* eslint-env node */
'use strict';

module.exports = function() {
  const io = require('socket.io')(4201);

  io.on('connection', function(socket) {
    socket.on('join', function({ room }) {
      if (socket.room) {
        socket.leave(socket.room);
      }
      socket.join(room);
      socket.room = room;
    });

    socket.on('disconnect', function() {
      if (!socket.clientID) { return; }
      socket.to(socket.room).emit('eviction', {
        address: socket.id,
        client: socket.clientID
      });
    });

    socket.on('message', function(payload) {
      socket.to(socket.room).emit('message', payload);
    });

    socket.on('register-client', function({ client }) {
      socket.clientID = client;
    });

    socket.room = socket.id;
    socket.emit('address-assignment', { address: socket.id });
  });
};
