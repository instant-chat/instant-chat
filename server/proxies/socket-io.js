/* eslint-env node */
'use strict';

const proxyPath = 'socket.io';

module.exports = function(app) {
  // For options, see:
  // https://github.com/nodejitsu/node-http-proxy
  let proxy = require('http-proxy').createProxyServer();

  proxy.on('error', function(err, req) {
    console.error(err, req.url); // eslint-disable-line
  });

  app.use('/' + proxyPath, function(req, res){
    // include root path in proxied request
    req.url = proxyPath + '/' + req.url;
    let target = 'http://127.0.0.1:4201';
    proxy.web(req, res, { target, ws: true });
  });
};
