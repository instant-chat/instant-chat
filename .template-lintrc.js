/* eslint-env node */
'use strict';

module.exports = {
  extends: 'recommended',

  ignore: [
    './node_modules/**',
    './vendor/**'
  ],

  rules: {
    "no-bare-strings": true,
    "no-implicit-this": {
      allow: ['has-block', 'hasBlock', 'app-version']
    }
  },
};
